# Going beyond notebooks – tips around data science productivity

Presentation for ING's internal *Data Science Community Conference*.

## Abstract

There is a lot of attention already in the data science community on algorithms, projects, experiments and open source packages.

How you get things done is highly personal and rarely discussed. During this talk I want to inspire you to become more productive, and will demo and promote certain workflows.

We will cover the terminal, vim, git and gitlab workflows, how to structure projects, some more advanced python topics such as (data) classes and type hinting, a python editor (VSCode). After this talk you should have picked some new tricks and be inspired to learn more!



