import pytest
import numpy as np

from projectname import data

def test_load_data():

    X, y = data.load_data()
    assert X.shape[0] > 0
    assert type(y) == np.ndarray