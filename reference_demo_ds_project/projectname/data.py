
import pandas as pd
from sklearn.datasets import make_classification

def load_data() -> pd.DataFrame:

    X, y = make_classification(n_samples = 10000, n_features = 20, n_informative=15, n_redundant=2, n_classes=2, weights = [.9, .1], random_state = 42)
    X = pd.DataFrame(X)

    return X, y

