from setuptools import setup, find_packages

setup(
    name="projectname",
    description="Example project.",
    author="Your Name",
    packages=find_packages(exclude=['data', 'notebooks']),
)
