
import logging
logging.basicConfig(level=logging.INFO)

from projectname import data

import fire

import os
from dotenv import load_dotenv
load_dotenv()

def train_model(from_date = None, to_date = None):
    X, y = data.load_data()

    # fit your model
    # save trained model as a .pkl to data/ folder

    logging.info(os.getenv("spark_cluster"))
    logging.info('trained model.')

if __name__ == '__main__':

    fire.Fire(train_model)

